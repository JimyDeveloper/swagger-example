package com.example.swagger.controller;

import com.example.swagger.dto.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/users/")
@Api(value = "User Resource", description = "User operations API")
public class UserResource {

  @GetMapping("/get/{userId}")
  @ApiOperation(value = "Get User By Id")
  public User getUserById(@PathVariable @ApiParam(value = "enter user id as integer") long userId) {
    return new User(userId, "To'lqin");
  }

  @PostMapping("/create")
  @ApiOperation(value = "Create User")
  public User create(@RequestBody User user) {
    return user;
  }
}
