package com.example.swagger.configuration;

import static springfox.documentation.builders.PathSelectors.regex;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.example.swagger.controller"))
            .paths(regex("/api/users.*"))
            .build()
            .apiInfo(metaInfo());
    }

    private ApiInfo metaInfo() {

        ApiInfo apiInfo = new ApiInfo(
            "Spring Boot Swagger Example API",
            "Spring Boot Swagger Example API for Example",
            "1.0",
            "Terms of Service",
            new Contact("Examplr", "https://www.youtube.com/example",
                "example@gmail.com"),
            "Apache License Version 2.0",
            "https://www.apache.org/licesen.html"
        );

        return apiInfo;
    }
}