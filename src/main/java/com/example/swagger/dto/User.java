package com.example.swagger.dto;

import io.swagger.annotations.ApiParam;
import lombok.Data;

@Data
public class User {

  @ApiParam(value = "id as integer")
  private long id;
  @ApiParam(value = "name as string")
  private String name;

  public User(long id, String name) {
    this.id = id;
    this.name = name;
  }
}
